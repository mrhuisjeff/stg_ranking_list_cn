module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  extends: 'airbnb-base',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // check if imports actually resolve
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'build/webpack.base.js'
      }
    },
  },
  // add your custom rules here
  'rules': {
    //允许在内部require，以实现client端组件按需加载
    'global-require': 0,
    'linebreak-style':0,
    // don't require .vue extension when importing
    'import/extensions': ['error', 'never'],
    "indent": ["error", 4, {
      "SwitchCase": 1
    }],
    "import/prefer-default-export": 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    'no-console': process.env.NODE_ENV === 'production' ? 2 : 0,
    // 允许大写字母开头的函数可以不是构造函数
    'new-cap': ['error', {
      'capIsNew': false
    }],
  }
}
