* All should be done under alpha folder

- global node cli install
npm install -g gulp-cli     # for gulp
npm install -g webpack webpack-dev-server   # for webpack

- dev env setting
npm install

- start webpack watch
webpack --progress --watch

- start gulp for compiling sass
gulp sass & gulp sass:watch

- start local node server
node bin/www

- Deploy for QA or Production
