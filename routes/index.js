const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const request = require('request');
const cache = require('memory-cache');
const config = require('../config');

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index');
// });

router.post('/login', function(req, res, next){
    if(!req.body.username || !req.body.password){
        res.json({
            status: false,
            msg: 'username or password not given'
        });
        return;
    }
    // get authorization from the original api
    const cookieJar = request.jar();
    request.post({
        url: config.Route.Login,
        jar: cookieJar,
        form: {
            account: req.body.username,
            password: req.body.password
        }
    }, function(err, response, body){

        console.log(cookieJar.getCookieString(config.api));
        console.log(JSON.parse(body));
        const parsedBody = JSON.parse(body);
        if(!parsedBody.status){
            // login failed
            res.json({
                status: false,
                msg: 'login credential invalid'
            });
            return;
        }
        // login success
        const cookieStr = cookieJar.getCookieString(config.api);
        const sessionId = cookieStr.split('=')[1];
        console.log(cookieStr);
        const token = jwt.sign({sid: sessionId}, config.serverSecret);
        res.send({
            status: true,
            username: parsedBody.data.name,
            token: token
        })
        res.end();
    });
});

router.post('/logout', function(req, res, next){
    res.end();
});

router.get('/game', function(req, res, next){
    res.end();
});

router.get('/game/tree', function(req, res, next){
    if (cache.get('api:game:tree')) {
        res.json(cache.get('api:game:tree'));
        res.end();
        return;
    }
    request.get({
        url: config.Route.GameTree
    }, function(err, response, body){
        cache.put('api:game:tree', JSON.parse(body), config.cacheExpired);
        res.json(JSON.parse(body));
        res.end();
    })
});

router.get('/platform', function(req, res, next) {
  request.get({
    url: config.Route.Platform,
    qs: {
      gid: req.query.gameId
    }
  }, function(err, response, body) {
    res.json(JSON.parse(body));
    res.end();
  });
});

router.get('/news', function(req, res, next) {
    if (cache.get('api:news')) {
        res.json(cache.get('api:news'));
        res.end();
        return;
    }
    request.get({
        url: config.Route.News
    }, function(err, response, body) {
        cache.put('api:news', JSON.parse(body), config.cacheExpired);
        res.json(JSON.parse(body));
        res.end();
    });
});

router.get('/rank', function(req, res, next) {
    request.get({
        url: config.Route.Rank,
        qs: req.query
    }, function(err, response, body) {
        res.json(JSON.parse(body));
        res.end();
    });
});

router.post('/rank/submit', function(req, res, next) {
  const { token } = req.query;
  jwt.verify(token, config.serverSecret, function(err, decoded) {
    if (err) {
      return res.stats(402).end();
    }

    const { sid } = decoded;

    const cookieJar = request.jar();
    const cookie = request.cookie(`PHPSESSID=${sid}`);

    cookieJar.setCookie(cookie, config.api);

    request.post({
      url: config.Route.Submit,
      form: req.body,
      jar: cookieJar
    }, function(err, response, body) {
      console.log(body);
      res.json(JSON.parse(body));
      res.end();
    });
  });
});

module.exports = router;
