var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var nodemon = require('gulp-nodemon');

gulp.task('sass', function () {
    return gulp.src('./src/css/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({includePaths: ['./src/css']}).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./public'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./src/**/*.scss', ['sass']);
});

gulp.task('app', function() {
    return nodemon({
        script: './app.js',
        env: { 'NODE_ENV': 'development' },
        watch: [
            'routes',
            'config.js',
            'app.js',
        ]
    })
    //have nodemon run watch on start
    .on('restart', function() {
        gutil.log('Server restarted');
    });
});

gulp.task('default', ['sass', 'sass:watch', 'app'], function() {
  // place code for your default task here
});
