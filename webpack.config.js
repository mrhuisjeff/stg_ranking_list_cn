var webpack = require('webpack');
var path = require('path');
var srcPath = path.join(__dirname, 'src');

var webpackConfig = {
    devtool:'source-map',
    context: srcPath,
    entry: path.join(srcPath, 'js', 'client.js'),
    output: {
            publicPath:'public/',
            path: '/',
            filename: "bundle.js"
    },
    module: {
            loaders: [
                    {
                        test: /\.js?$/,
                        exclude: /(server|node_modules|bower_components)/,
                        loader: 'babel',
                        query: {
                            presets: ['es2015', 'stage-0', 'react']
                        }
                    },
            ]
    },
    resolve: {
        root: path.resolve('./'),
        extensions: ['', '.js', '.jsx']
    },

    plugins: []
};

if(process.env.NODE_ENV == 'production'){
    webpackConfig.output.path = './public';
    // webpackConfig.output.publicPath = 'build/public/';
    webpackConfig.vendor = [
        'react',
        'react-router',
        'react-redux',
    ];
    webpackConfig.plugins.push(
        new webpack.DefinePlugin(
            {'process.env': {
                NODE_ENV: JSON.stringify('production')
                }
            }
    ));

    webpackConfig.plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = webpackConfig;
