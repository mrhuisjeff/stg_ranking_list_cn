/**
 *  file name: config.js
 *  author: Gabriel Yum
 *  date: 2016-10-25
 *  usage:
 *      config file
 */

const globalConfigs = {
    production:{
        api: 'http://ranking.niyaozao.com/frontapi/api',
        serverSecret: 'niyaozao',
        cacheExpired: 3600 * 1000,
        contextPath: '/qa',
    },
    qa: {
        api: 'http://ranking.niyaozao.com/test_stg/frontapi/api',
        serverSecret: 'niyaozao',
        cacheExpired: 3600 * 1000,
        contextPath: '/qa',
    },
    development: {
        api: 'http://ranking.niyaozao.com/test_stg/frontapi/api',
        serverSecret: 'niyaozao',
        cacheExpired: 3600 * 1000,
        contextPath: '',
    }
}

let config = {
    Route: {}
};

if (!globalConfigs[process.env.NODE_ENV]) {
    config = Object.assign(config, globalConfigs.development);
} else {
    config = Object.assign(config, globalConfigs[process.env.NODE_ENV]);
}

const constantConfigs = {
  Route: {
    Login: '/public/dologin',
    Logout: '/',
    Game: '/game',
    GameTree: '/game/getDataTree',
    News: '/rank/getDataNear',
    Rank: '/rank/getData',
    Platform: '/space/getData',
    Submit: '/rank/submit'
  }
};

// give prefix for the routes

Object.keys(constantConfigs.Route).map(key => {
    config.Route[key] = config.api + constantConfigs.Route[key];
});

module.exports = config;
