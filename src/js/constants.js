export default {
    App: {
        INIT_APP: 'INIT_APP',
        UPDATE_USER: 'UPDATE_USER'
    },
    Rank: {
        UPDATE_RANKS: 'UPDATE_RANKS'
    },
    Game: {
        INIT_GAMES: 'INIT_GAMES',
        UPDATE_GAME_STATE: 'UPDATE_GAME_STATE'
    },
	Brand: {
		INIT_BRANDS: 'INIT_BRANDS',
	},
    News: {
        UPDATE_NEWS: 'UPDATE_NEWS',
    }
}
