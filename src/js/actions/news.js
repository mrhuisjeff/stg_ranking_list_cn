/**
 *  file name: news.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      actions for games & brands
 */

import request from 'superagent';
import ActionTypes from '../constants';

export function updateNews() {
    return (dispatch, state) => {
        request
            .get(window.contextPath+'/news')
            .end(function(err, res) {
                if (err) {
                    return;
                }

                dispatch({
                    type: ActionTypes.News.UPDATE_NEWS,
                    news: res.body
                })
            });
    };
}
