/**
 *  file name: game.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      actions for games & brands
 */

import request from 'superagent';
import ActionTypes from '../constants';

const ROWS_PER_PAGE = 10;

export function updateRanks(gameId, page) {
    return (dispatch, getState) => {
        const state = getState();

        request
            .get(window.contextPath+'/rank')
            .query({gid: gameId, page: page, rows: ROWS_PER_PAGE})
            .end(function(err, res) {
                if (err) {
                    return;
                }

                // update ranks
                const ranks = res.body.reduce((map, rank) => {
                    rank.gameId = gameId;
                    map[rank.id] = rank;
                    return map;
                }, {});
                
                dispatch({
                    type: ActionTypes.Rank.UPDATE_RANKS,
                    ranks
                });

                // update game states for the rank
                const rankIds = res.body.map(rank => {
                    return rank.id;
                });
                const originalRankList = state.games[gameId].state.ranks;

                dispatch({
                    type: ActionTypes.Game.UPDATE_GAME_STATE,
                    gameId,
                    state: {
                        init: true,
                        complete: rankIds.length < ROWS_PER_PAGE,
                        page,
                        ranks: rankIds
                    }
                });
            });
    };
}
