// file: app.js
import Cookies from 'js-cookie';
import request from 'superagent';
import ActionTypes from '../constants';

export function initApp() {
    return (dispatch) => {
        // fetch games data
        request.get(window.contextPath+'/game/tree')
            .then((res) => {
                const games = {};
                const brands = res.body.reduce((brandMap, brand) => {
					const brandGames = brand.games;
					// games could be null instead of empty array
					if (brandGames) {
                        brand.games = brandGames.map((game) => {
                            games[game.id] = game;
                            return game.id;
                        });
					} else {
                        brand.games = [];
                    }
					brandMap[brand.id] = brand;
					return brandMap;
				}, {});

                dispatch({
					type: ActionTypes.Brand.INIT_BRANDS,
					brands
				});

                dispatch({
                    type: ActionTypes.Game.INIT_GAMES,
                    games
                });
            })
            .then(() => {
                // fetch news data
                return request.get(window.contextPath+'/news');
            })
            .then((res) => {
                dispatch({
                    type: ActionTypes.News.UPDATE_NEWS,
                    news: res.body
                });
            })
            .then(() => {
                // fetch Cookies
                const session = Cookies.get('session');

                if (session) {
                  console.log(JSON.parse(session));
                  dispatch({
                    type: ActionTypes.App.UPDATE_USER,
                    user: JSON.parse(session)
                  })
                }
                // finish init the app
                dispatch({
                    type: ActionTypes.App.INIT_APP
                });
            });
    };
}

export function login(username, password) {
  return (dispatch) => {
    request.post(window.contextPath + '/login')
      .send({
        username,
        password
      })
      .then((res) => {
        const response = res.body;
        if (!response.status) {
          return;
        }
        const { username, token } = response;
        const user = {
          username,
          token
        };
        // write to Cookies
        Cookies.set('session', JSON.stringify(user));
        // pass to reducer
        dispatch({
          type: ActionTypes.App.UPDATE_USER,
          user
        });
      });
  };
}

export function logout(username, password) {
  return (dispatch) => {
    // clear cookie
    Cookies.remove('session');
    dispatch({
      type: ActionTypes.App.UPDATE_USER,
      user: null
    });
  };
}
