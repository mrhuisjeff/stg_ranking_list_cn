'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { IndexRoute, Router, Route, browserHistory } from 'react-router';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

import App from './components/App';
import Index from './components/Index';
import Game from './components/Game';
import NotFound from './components/NotFound';
import Style from './components/Style';
import Submit from './components/Submit';
import reducer from './reducers';

// Needed for onTouchTap
// injectTapEventPlugin();

const logger = createLogger();
const store = createStore(
	reducer,
    applyMiddleware(thunk)
	// applyMiddleware(thunk, logger)
);

const app = document.getElementById('app');

ReactDOM.render(
    <Provider store={store}>
      <Router history={browserHistory}>
        <Route path="/style" component={Style} />
          <Route path="/" component={App}>
              <IndexRoute component={Index} />
							<Route path="submit" component={Submit} />
              <Route path=":brandSlug/:gameSlug" component={Game} />
              <Route path="*" component={NotFound} />
          </Route>
      </Router>
    </Provider>
, app);
