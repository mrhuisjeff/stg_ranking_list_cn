/**
 *  file name: app.js
 *  author: Gabriel Yum
 *  date: 2016-10-24
 *  usage:
 *      Index page for this app
 */

'use strict';

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import GameMenu from './GameMenu';
import Header from './Header';

import * as appActionCreator from '../actions/app';
import { appSelector } from '../selectors/app';

class AppComponent extends React.Component {
	contextTypes: {
      router: React.PropTypes.func.isRequired,
    }

	constructor(props) {
		super(props);
	}

    componentDidMount() {
        // init the app if necessary
        const { appActions, app } = this.props;

        if (!app.init) {
            appActions.initApp();
        }
    }

	render() {
        const { app, params } = this.props;

        if (!app.init) {
            return (
                <div>
                    <h1>Loading...</h1>
                </div>
            );
        }

		return (
			<div className="container">
                <Header {...this.props}/>

				<div className="columns gapless">
					<div className="column is-3 game-menu">
            <GameMenu params={params}/>
          </div>

					<div className="column is-9">
		      	        {this.props.children}
					</div>

				</div>
			</div>
		);
	}
}


const mapDispatchToProps = (dispatch) => ({
    appActions: bindActionCreators(appActionCreator, dispatch),
});

export default connect(appSelector, mapDispatchToProps)(AppComponent);
