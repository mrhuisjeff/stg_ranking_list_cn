/**
 *  file: GameMenuItem.js
 */

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import request from 'superagent';

import * as gamesActionCreator from '../actions/games';
import { submitSelector } from '../selectors/games';

const initState = {
  brandId: '',
  gameId: '',
  model: '',
  type: '',
  score: '',
  platformId: '',
  stage: '',
  userName: '',
  userBid: '',
  comment: '',
  location: '',
  source: '',
  // non-form attributes
  brand: null,
  game: null,
  platform: null,
  gameList: [],
  modelList: [],
  typeList: [],
  platformList: [],
  submitAttempted: false,
  showNotification: false,
  notificationState: null,
  notificationMessage: ''
};

class SubmitComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = initState;

      this.handleBrandChange = ::this.handleBrandChange;
      this.handleGameChange = ::this.handleGameChange;
      this.handleModelChange = ::this.handleModelChange;
      this.handleTypeChange = ::this.handleTypeChange;
      this.handlePlatformChange = ::this.handlePlatformChange;
      this.handleScoreChange = ::this.handleScoreChange;
      this.handleStageChange = ::this.handleStageChange;
      this.handleUserNameChange = ::this.handleUserNameChange;
      this.handleBidChange = ::this.handleBidChange;
      this.handleLocationChange = ::this.handleLocationChange;
      this.handleCommentChange = ::this.handleCommentChange;
      this.handleSourceChange = ::this.handleSourceChange;
      this.handleSubmit = ::this.handleSubmit;
      this.handleNotificationClose = ::this.handleNotificationClose;
    }

    componentDidMount() {
      const { brands, games } = this.props;
      const brandList = Object.keys(brands).map(brandId => brands[brandId]);
      const brand = brandList.length === 0 ? null : brandList[0];
      const gameList = this.getGameList(brand);
      this.setState({
        brandId: brand.id,
        brand,
        gameList
      });
    }

    handleBrandChange(event) {
      const games = this.props.games;
      const brandId = event.target.value;
      const brand = this.props.brands[brandId];
      const gameList = this.getGameList(brand);
      this.setState({
        brand,
        brandId,
        gameList
      });
    }

    handleGameChange(event) {
      const { fetchGamePlatform } = this.props.gameActions;
      const gameId = event.target.value;
      const game = this.props.games[gameId];
      const modelList = game.model.split(',');
      const typeList = game.type.split(',');

      request.get(`${window.contextPath}/platform?gameId=${gameId}`)
        .then(response => {
          const { body } = response;
          const platformList = body;
          this.setState({
            gameId,
            game,
            modelList,
            typeList,
            platformList
          });
        });
    }

    handleModelChange(event) {
      const model = event.target.value;
      this.setState({
        model
      });
    }

    handleTypeChange(event) {
      const type = event.target.value;
      this.setState({
        type
      });
    }

    handlePlatformChange(event) {
      const platformId = event.target.value;
      const platform = this.state.platformList.find(p => p.id === platformId);
      this.setState({
        platform,
        platformId
      });
    }

    handleScoreChange(event) {
      this.setState({
        score: event.target.value
      });
    }

    handleStageChange(event) {
      this.setState({
        stage: event.target.value
      });
    }

    handleUserNameChange(event) {
      this.setState({
        userName: event.target.value
      });
    }

    handleBidChange(event) {
      this.setState({
        userBid: event.target.value
      });
    }

    handleLocationChange(event) {
      this.setState({
        location: event.target.value
      });
    }

    handleCommentChange(event) {
      this.setState({
        comment: event.target.value
      });
    }

    handleSourceChange(event) {
      this.setState({
        source: event.target.value
      });
    }

    handleSubmit(event) {
      const {
        score,
        gameId,
        model,
        type,
        platformId,
        userName,
        userBid,
        comment,
        stage,
        location,
        source
      } = this.state;
      const { token } = this.props.app.user;
      const { brands, games } = this.props;


      const data = {
        score,
        game_id: gameId,
        model,
        type,
        space_id: platformId,
        user_name: userName,
        user_bid: userBid,
        comment,
        stage,
        location,
        time: this.getDatetime(),
        source: source
      };

      request
        .post(`/rank/submit?token=${token}`)
        .send(data)
        .end((err, res) => {
          if (err) {
            return console.log(err);
          }
          const { status } = res.body;

          if (status) {
            const brandList = Object.keys(brands).map(brandId => brands[brandId]);
            const brand = brandList.length === 0 ? null : brandList[0];
            const gameList = this.getGameList(brand);
            this.setState({
            });
            this.setState({
              ...initState,
              brandId: brand.id,
              brand,
              gameList,
              showNotification: true,
              notificationState: 'success',
              notificationMessage: '已成功提交成绩!'
            });
          } else {
            this.setState({
              showNotification: false,
              notificationState: 'failure',
              notificationMessage: '成绩提交失败，请重试'
            });
          }
        });
    }

    handleNotificationClose(event) {
      this.setState({
        showNotification: false,
        notificationState: null,
        notificationMessage: null
      });
    }

    getGameList(brand) {
      const { games } = this.props;
      return brand.games.map(gameId => games[gameId]);
    }

    getDatetime() {
      const dt = new Date();
      return `${dt.getFullYear()}-${dt.getMonth() + 1}-${dt.getDate()}`;
    }

    render() {
      // console.log(this.props);
      const { gameList, modelList, typeList, platformList, showNotification, notificationState, notificationMessage } = this.state;
      const { brands } = this.props;

      // brand options
      const brandOptions = Object.keys(brands).map(brandId => {
        const brand = brands[brandId];

        return (
          <option key={brandId} value={brandId}>{brand.name}</option>
        );
      });
      // game options
      const availableGameOptions = gameList.map(game => {
        return (
          <option key={game.id} value={game.id}>{game.name}</option>
        );
      });

      const emptyGameOption = (
        <option key="-1" disabled defaultValue>-- 请选择游戏 --</option>
      );

      const gameOptions = [emptyGameOption, ...availableGameOptions];
      // model options
      const availableModelOptions = modelList.map(model => {
        return (
          <option key={model} value={model}>{model}</option>
        );
      });

      const emptyModelOption = (
        <option key="-1" disabled defaultValue>-- 请选择模式 --</option>
      );

      const modelOptions = [emptyModelOption, ...availableModelOptions];

      // type options
      const availableTypeOptions = typeList.map(type => {
        return (
          <option key={type} value={type}>{type}</option>
        );
      });

      const emptyTypeOption = (
        <option key="-1" disabled defaultValue>-- 请选择机体 --</option>
      );

      const typeOptions = [emptyTypeOption, ...availableTypeOptions];

      // platform options
      const availablePlatformOptions = platformList.map(platform => {
        return (
          <option key={platform.id} value={platform.id}>{platform.name}</option>
        );
      });

      const emptyPlatformOption = (
        <option key="-1" disabled defaultValue>-- 请选择平台 --</option>
      );

      const platformOptions = [emptyPlatformOption, ...availablePlatformOptions];

      // score field
      const scoreFieldCls = classNames({
        'input': true,
        'is-danger': this.state.submitAttempted && (typeof this.state.score !== 'string' || this.state.score.length === 0)
      });

      // stage field
      const stageFieldCls = classNames({
        'input': true,
        'is-danger': this.state.submitAttempted && (typeof this.state.stage !== 'string' || this.state.stage.length === 0)
      });

      // userName field
      const userNameFieldCls = classNames({
        'input': true,
        'is-danger': this.state.submitAttempted && (typeof this.state.stage !== 'string' || this.state.stage.length === 0)
      });

      // userBid field
      const userBidFieldCls = classNames({
        'input': true,
        'is-danger': this.state.submitAttempted && (typeof this.state.stage !== 'string' || this.state.stage.length === 0)
      });

      // location field
      const locationFieldCls = classNames({
        'input': true,
        'is-danger': this.state.submitAttempted && (typeof this.state.stage !== 'string' || this.state.stage.length === 0)
      });


      // datetime field
      const currentDateTimeStr = this.getDatetime();

      // notification message
      const notificationCls = classNames({
        'notification': true,
        'submit-page-notification': true,
        'is-success': notificationState === 'success',
        'is-danger': notificationState === 'failure',
        'submit-page-notification--invisible': !showNotification
      });


      return (
        <section className="section submit-page">
            <div className="heading">
              <div className="title">提交成绩</div>
            </div>

            <div className={notificationCls}>
              <button className="delete" onClick={this.handleNotificationClose}></button>
              {notificationMessage}
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">厂商</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <div className="select">
                      <select value={this.state.brandId} onChange={this.handleBrandChange}>
                        {brandOptions}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">游戏</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <div className="select">
                      <select value={this.state.gameId} onChange={this.handleGameChange}>
                        {gameOptions}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">模式</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <div className="select">
                      <select value={this.state.model} onChange={this.handleModelChange}>
                        {modelOptions}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">机体</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <div className="select">
                      <select value={this.state.type} onChange={this.handleTypeChange}>
                        {typeOptions}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">分数</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <input type="text" className={scoreFieldCls} onChange={this.handleScoreChange}/>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">平台</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <div className="select">
                      <select value={this.state.platformId} onChange={this.handlePlatformChange}>
                        {platformOptions}
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">到达关卡</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <input type="text" className={stageFieldCls} onChange={this.handleStageChange}/>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">玩家名</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <input type="text" className={userNameFieldCls} onChange={this.handleUserNameChange}/>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">玩家机签</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <input type="text" className={userBidFieldCls} onChange={this.handleBidChange}/>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">成绩获得地</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <input type="text" className={locationFieldCls} onChange={this.handleLocationChange}/>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">玩家感言</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <textarea className="textarea" onChange={this.handleCommentChange}/>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">成绩时间</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <input type="text" className="input" value={currentDateTimeStr} readOnly disabled/>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"><label className="label">成绩资源</label></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <input type="text" className="input" onChange={this.handleSourceChange}/>
                  </div>
                </div>
              </div>
            </div>

            <div className="field is-horizontal">
              <div className="field-label"></div>
              <div className="field-body">
                <div className="field">
                  <div className="control">
                    <button className="button is-primary" onClick={this.handleSubmit}>提交</button>
                  </div>
                </div>
              </div>
            </div>
        </section>
      )
    }
}

const mapDispatchToProps = (dispatch) => ({
	gameActions: bindActionCreators(gamesActionCreator, dispatch),
});

export default connect(submitSelector, mapDispatchToProps)(SubmitComponent);
