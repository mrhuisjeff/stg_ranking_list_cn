import React from 'react';
import classNames from 'classnames';

import RankFilterItem from './RankFilterItem';

export default class RankFilter extends React.Component  {
  render() {
    const { game, filters, updateFilter } = this.props;

    const filterItems = filters.map(filter => <RankFilterItem key={filter.type} filter={filter} updateFilter={updateFilter} />);

    return (
      <div className="rank-filter">
        {filterItems}
      </div>
    );
  }
}
