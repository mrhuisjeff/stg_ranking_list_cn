/**
 *  file name: menu.jsx
 *  author: Gabriel Yum
 *  date: 2016-10-25
 *  usage:
 *      Menu component
 */

'use strict';

import React from 'react';
import { Link,browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';

import * as appActionCreator from '../actions/app';

import { appSelector } from '../selectors/app';


class LoginComponent extends React.Component {

    constructor(props) {
        super(props);

      this.state = {
        loginDialogOpen: false,
        username: 'haloyum',
        password: 'abcd1234'
      };

      this.handleUsernameChange = ::this.handleUsernameChange;
      this.handlePasswordChange = ::this.handlePasswordChange;
      this.handleLogin = ::this.handleLogin;
      this.handleLogout = ::this.handleLogout;
      this.handleDialogOpen = ::this.handleDialogOpen;
      this.handleDialogClose = ::this.handleDialogClose;
    }

    handleUsernameChange(e) {
      this.setState({
        username: e.target.value
      });
    }

    handlePasswordChange(e) {
      this.setState({
        password: e.target.value
      });
    }

    handleLogin() {
      const { appActions } = this.props;
      const { username, password } = this.state;

      appActions.login(username, password);
      this.setState({
        loginDialogOpen: false
      });
    }

    handleLogout(e) {
      e.preventDefault();

      const { appActions } = this.props;

      appActions.logout();
    }

    handleDialogOpen(e) {
      e.preventDefault();
      this.setState({
        loginDialogOpen: true
      });
    }

    handleDialogClose(e) {
      e.preventDefault();
      this.setState({
        loginDialogOpen: false
      });
    }
    componentWillReceiveProps(nextProps){
      if(JSON.stringify(nextProps.app.user) !== JSON.stringify(this.props.app.user)){
        browserHistory.push('/');
      }
    }
    render() {
      const { loginDialogOpen, username, password } = this.state;
      const { app, params } = this.props;
      const { user } = app;
      const loggedIn = user !== null;

      const loginDialogCls = classNames({
        'modal': true,
        'is-active': loginDialogOpen
      });

      const profileMenu = loggedIn ?
      (
        <div className="game-menu-profile-registered">
          <a className="button is-white">{user.username}</a>
          <button className="button is-primary header-user-button">
            <Link to="/submit">提交成绩</Link>
          </button>
          <a className="button is-danger header-user-button" onClick={this.handleLogout}>登出</a>
          <a className="button is-link" target="_blank" href="http://http://www.niyaozao.com/">访问论坛</a>
        </div>
      ) :
      (
        <div>
            <a className="button is-primary header-user-button">注册</a>
            <a className="button is-primary header-user-button" href="" onClick={this.handleDialogOpen}>登陆</a>
            <a className="button is-link" target="_blank" href="http://http://www.niyaozao.com/">访问论坛</a>
        </div>
      );

        return (
            <div className="loginContainer">
              <div className={loginDialogCls}>
                <div className="modal-background"></div>
                <div className="modal-content">
                  <div className="section">
                    <div className="field">
                      <label className="label">用户名</label>
                      <p className="control">
                        <input className="input" type="text" onChange={this.handleUsernameChange}/>
                      </p>
                      <label className="label">密码</label>
                      <p className="control">
                        <input className="input" type="password" onChange={this.handlePasswordChange}/>
                      </p>
                    </div>
                    <div className="field">
                      <p className="control">
                        <a className="button is-primary" onClick={this.handleLogin}>提交</a>
                      </p>
                    </div>
                  </div>
                </div>
                <button className="modal-close" onClick={this.handleDialogClose}></button>
              </div>
                {profileMenu}
                
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    appActions: bindActionCreators(appActionCreator, dispatch)
});

export default connect(appSelector, mapDispatchToProps)(LoginComponent);
