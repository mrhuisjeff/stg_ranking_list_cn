/**
 *  file name: menu.jsx
 *  author: Gabriel Yum
 *  date: 2016-10-25
 *  usage:
 *      Menu component
 */

'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';

import * as appActionCreator from '../actions/app';

import { gameMenuSelector } from '../selectors/games';

import GameMenuItem from './GameMenuItem';

class GameMenuComponent extends React.Component {

    constructor(props) {
        super(props);
    }
    render() {
      const { app, brands, games, params } = this.props;
      const brandItems = Object.keys(brands).map(id => {
          const brand = brands[id];
          return (
              <GameMenuItem key={brand.id} brand={brand} games={games} params={params}/>
          );
      });

        return (
            <div className="column">
                <div className="menu">
                  <ul className="menu-list">
                    {brandItems}
                  </ul>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    appActions: bindActionCreators(appActionCreator, dispatch)
});

export default connect(gameMenuSelector, mapDispatchToProps)(GameMenuComponent);
