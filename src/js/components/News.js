/**
 *  file: LatestRank.js
 */

//  {
// "id": "309",
// "score": "58753100",
// "user_name": "姚雨和",
// "user_bid": "FEI",
// "comment": "三十秒推把模式捡个漏+1",
// "stage": "ALL",
// "model": "Time Attack",
// "type": "Type-I",
// "loaction": "广州",
// "source": "",
// "time": "2015-11-08",
// "game_id": "34",
// "game_name": "红莲四羽 星火燎原",
// "space_name": "PC-steam",
// "rank": 1
// },

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { newsSelector } from '../selectors/news';
import * as newsActionCreator from '../actions/news';

import { thousandBitSeparator } from '../libs/utils.js';

class NewsComponent extends React.Component {

    render() {
        const { news } = this.props;
        // const newsItems = null;
        const newsItems = news.map((news, i) => {
            return (
                <p key={i}>
                    <span className="date">{news.time}</span> &nbsp;
                    <span>{news.user_name} 在{news.game_name}中取得了{thousandBitSeparator(news.score)}分. </span>
                </p>
            );
        });

        return (
            <section className="section">
              <div className="container">
                <div className="heading">
                  <h1>最新上榜</h1>
                </div>
              </div>
              {newsItems}
            </section>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
	newsActions: bindActionCreators(newsActionCreator, dispatch),
});

export default connect(newsSelector, mapDispatchToProps)(NewsComponent);
