import React from 'react';

export default class Style extends React.Component {
    render() {
        return (
            <div>
                <h2>Style Guide Page</h2>
                <div>
                    <button>Normal Button</button>
                </div>
                <br></br>
                <div>
                    <a><button>Redirect Button</button></a>
                </div>
            </div>
        );
    }
}
