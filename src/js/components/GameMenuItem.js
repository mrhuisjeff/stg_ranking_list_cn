/**
 *  file: GameMenuItem.js
 */

import React from 'react';
import { Link } from 'react-router';
import classNames from 'classnames';

export default class GameMenuItem extends React.Component {

    // static propTypes

    constructor(props) {
        super(props);

        // bind the event
        this.handleClick = ::this.handleClick;

        // set the state
        this.state = {
          open: false
        };
    }

    componentWillUpdate(prevProps, prevstate) {
      if (prevProps.params != this.props.params) {
        this.state.open = false;
      }
    }

    handleClick(event) {
      this.setState({
        open: !this.state.open
      });
      event.preventDefault();
    }

    render() {
      const { brand, games } = this.props;
      const { open } = this.state;

      const brandItemCls = open ? '' : 'game-menu-brand--invisible';

      const brandGameMenuItems = brand.games.map(gameId => {
        const game = games[gameId];

        return (
          <li key={gameId} className="menu-item">
            <Link to={`/${brand.slug}/${game.slug}`}>
              {game.name}
            </Link>
          </li>
        );
      });

      return (
        <li key={brand.id}>
          <a href="" onClick={this.handleClick}>{brand.name}</a>
          <ul className={brandItemCls}>{brandGameMenuItems}</ul>
        </li>
      );
    }
}
