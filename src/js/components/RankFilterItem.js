import React from 'react';
import classNames from 'classnames';

class RankFilterItemButton extends React.Component {
  constructor(props) {
    super(props);

    this.onButtonClick = ::this.onButtonClick;
  }

  onButtonClick() {
    const { value, filter, updateFilter } = this.props;
    updateFilter({ filter, value });
  }

  render() {
    const { filter, value } = this.props;

    const cls = classNames({
      'button': true,
      'rank-filter-item--option': true,
      'is-light': value !== filter.selected,
      'is-danger': value === filter.selected
    });

    return <a className={cls} onClick={this.onButtonClick}>{value}</a>;
  }
}

export default class RankFilterItem extends React.Component {
  render() {
    const { filter, updateFilter } = this.props;

    const options = filter.values.map(value => {
      return (<RankFilterItemButton key={value}
                                    filter={filter}
                                    value={value}
                                    updateFilter={updateFilter}>
                                    {value}</RankFilterItemButton>);
    });

    return (
      <div className="rank-filter-item">
        <span className="rank-filter-item--name">{filter.name}:</span>
        <div className="rank-filter-item--options">
          {options}
        </div>
      </div>
    );
  }
}
