import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import * as appActionCreator from '../actions/app';
import { appSelector } from '../selectors/app';

import LoginComponent from './Login';

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="header columns gapless">
        <div className="column is-2 title-img">
          <Link to="/">
            <img src="/img/logo.png"/>
          </Link>
        </div>

        <div className="column is-6 title-text">
          欢迎来到全国主流街机STG高分榜
        </div>

        <div className="column is-4 user-section">
          <LoginComponent {...this.props} />
        </div>
    </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
    appActions: bindActionCreators(appActionCreator, dispatch)
});

export default connect()(Header);
