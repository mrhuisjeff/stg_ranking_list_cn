/**
 * file: Index.js
 *
 */

import React from 'react';
import News from './News';

export default class IndexComponent extends React.Component {
    render() {
        return (
          <div>
            <section className="section">
            <figure className="image"><img src="https://uc.uxpin.com/files/311355/315818/homepagestg-2.png" alt="Hello World"></img></figure>
            </section>
            <News />
          </div>
        );
    }
}
