import React from 'react';

export default class NotFoundComponent extends React.Component {
    render() {
        return (
          <div className="not-found">
            <h2>The page does not exist</h2>
          </div>
        );
    }
}
