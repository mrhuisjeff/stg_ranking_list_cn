import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import request from 'superagent';

import { gameSelector } from '../selectors/games';
import * as ranksActionCreator from '../actions/ranks';

import { thousandBitSeparator } from '../libs/utils';

import RankFilter from './RankFilter';

const IMAGE_DOMAIN = 'http://ranking.niyaozao.com';

const DEFAULT_FILTER = '全部';

class GameComponent extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        filters: [
          {
            type: 'type',
            name: '机体',
            key: 'type',
            selected: DEFAULT_FILTER,
            values: [DEFAULT_FILTER]
          },
          {
            type: 'platform',
            name: '平台',
            key: 'spaceName',
            selected: DEFAULT_FILTER,
            values: [DEFAULT_FILTER]
          }
        ]
      };

      this.onLoadMore = ::this.onLoadMore;
      this.loadGameFilters = ::this.loadGameFilters;
      this.updateFilter = ::this.updateFilter;
    }

    componentWillReceiveProps(nextProps) {
      const { ranksActions } = this.props;
      if (nextProps.game.id !== this.props.game.id) {
        const game = nextProps.game;
        if (!game.state.init) {
            ranksActions.updateRanks(game.id, 1);
        }

        this.loadGameFilters(game);
      }
    }

    componentDidMount() {
      const { game, ranksActions} = this.props;
      const { state } = game;

      if (!state.init) {
          ranksActions.updateRanks(game.id, 1);   // page starts from one
      }

      this.loadGameFilters(game);
    }

    onLoadMore(e) {
      const { game, ranksActions } = this.props;
      ranksActions.updateRanks(game.id, game.state.page + 1);
    }

    loadGameFilters(game) {
      request.get(`${window.contextPath}/platform?gameId=${game.id}`)
        .then(response => {
          const platformList = response.body.map(platform => platform.name);
          const typeList = game.type.split(',');
          const filters = this.state.filters.map(filter => {
            if (filter.type === 'type') {
              filter.values = [DEFAULT_FILTER, ...typeList];
            } else if (filter.type === 'platform') {
              filter.values = [DEFAULT_FILTER, ...platformList];
            }
            return filter;
          });
          this.setState({ filters });
        });
    }

    updateFilter({filter, value} = {}) {
      const filters = this.state.filters.map(f => {
        if (filter.type === f.type) {
          f.selected = value;
        }

        return f;
      });

      this.setState({ filters });
    }

    render() {
        const { game } = this.props;
        const { filters } = this.state;
        const { state } = game;
        // check if game image is available
        const gameImage = game.image && game.image.length ?
            (<img src={`${IMAGE_DOMAIN}${game.image}`} alt="Hello World" />)
            : null;
        // prepare ranks
        const rankEntries = game.ranks.map((rank, index) => {
            let filtered = false;
            filters.forEach(filter => {
              filtered = filter.selected !== DEFAULT_FILTER && filter.selected !== rank[filter.key] || filtered;
            });

            if (filtered) {
              return null;
            }

            return (
                <li key={`rank-${game.id}-${index}`}>
                    <span className="group">
                        <span className="col-index"><label>排名</label>{index + 1}</span>
                        <span className="col-user-name"><label>机签</label>{rank.userName}</span>
                        <span className="col-score"><label>分数</label>{thousandBitSeparator(rank.score)}</span>
                    </span>
                    <span className="group">
                        <span className="col-stage"><label>到达关卡</label>{rank.stage}</span>
                        <span className="col-type"><label>机体</label>{rank.type}</span>
                        <span className="col-model"><label>模式</label>{rank.model}</span>
                    </span>
                    <span className="group">
                        <span className="col-space-name"><label>平台</label>{rank.spaceName}</span>
                        <span className="col-time"><label>时间</label>{rank.time}</span>
                        <span className="col-location"><label>成绩获得地</label>{rank.location}</span>

                    </span>
                    <span className="group">
                        <span className="col-comment"><label>玩家感言</label>{rank.comment}</span>
                    </span>
                </li>
            );
        });

        return (
            <div className="rank-content">
                {gameImage}
                <h2 className="rank-content--title">{game.name}</h2>
                <RankFilter game={game}
                            filters={filters}
                            updateFilter={this.updateFilter}/>
                <ul className="table">
                    <li className="table-header">
                        <span className="col-index">排名</span>
                        <span className="col-user-name">机签</span>
                        <span className="col-score">分数</span>
                        <span className="col-stage">到达关卡</span>
                        <span className="col-model">模式</span>
                        <span className="col-type">机体</span>
                        <span className="col-space-name">平台</span>
                        <span className="col-time">时间</span>
                        <span className="col-location">成绩获得地</span>
                        <span className="col-comment">玩家感言</span>
                    </li>
                    {rankEntries}
                </ul>
                <button className="button" disabled={state.complete} onClick={this.onLoadMore}>Load More</button>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    ranksActions: bindActionCreators(ranksActionCreator, dispatch)
});

export default connect(gameSelector, mapDispatchToProps)(GameComponent);
