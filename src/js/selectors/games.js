/**
 *  file name: game.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      selectors for games & brands
 */

import { createSelector } from 'reselect';

const appStateSelector = (state) => state.app;
const brandsStateSelector = (state) => state.brands;
const gamesStateSelector = (state) => state.games;

/* Selector for getting current game */
const gameStateSelector = (state, props) => {
    const { params } = props;
    const brands = state.brands;
    const games = state.games;
    const ranks = state.ranks;

    if (params) {
        const { brandSlug, gameSlug } = params;

        const selectedGameId = Object.keys(games).find(gameId => {
            const game = games[gameId];
            return game.slug == gameSlug;
        });

        const selectedBrandId = Object.keys(brands).find(brandId => {
            const brand = brands[brandId];
            return brand.slug == brandSlug;
        });

        const selectedGame = games[selectedGameId];
        const selectedBrand = brands[selectedBrandId];

        if (selectedGame && selectedBrand && selectedGame.brandId == selectedBrand.id) {
            selectedGame.ranks = selectedGame.state.ranks.map(rankId => {
                return ranks[rankId];
            });
            return selectedGame;
        }
    }

    return null;
};

export const gameSelector = createSelector(
    [gameStateSelector],
    (game) => ({
        game
    })
);

export const gameMenuSelector = createSelector(
    [appStateSelector, brandsStateSelector, gamesStateSelector],
    (app, brands, games) => ({
        app,
        brands,
        games
    })
)

export const submitSelector = createSelector(
  [appStateSelector, brandsStateSelector, gamesStateSelector],
  (app, brands, games) => ({
      app,
      brands,
      games
  })
)
