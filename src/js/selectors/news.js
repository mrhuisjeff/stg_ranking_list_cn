/**
 *  file name: news.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      selectors for games & brands
 */

import { createSelector } from 'reselect';

const newsStateSelector = (state) => state.news;

export const newsSelector = createSelector(
	[newsStateSelector],
	(news) => ({
		news
	})
);
