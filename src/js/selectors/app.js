/**
 *  file name: app.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      selectors for games & brands
 */

import { createSelector } from 'reselect';

const appStateSelector = (state) => state.app;

export const appSelector = createSelector(
	[appStateSelector],
	(app) => ({
		app
	})
);
