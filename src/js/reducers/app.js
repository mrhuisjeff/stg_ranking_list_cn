/**
 *  file name: games.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      reducer for games & brands
 */

import ActionTypes from '../constants';

// initial state
const initialState = {
    init: false,
    user: null
};

/**
 	{
		type: games.UPDATE_BRANDS
		games: {
			brand1: {
				game1: {...},
				...
			},
			...
		}
 	}
 */
function initApp(state, action) {
	return {
		...state,
    init: true
	};
}

function updateUser(state, action) {
  return {
    ...state,
    user: action.user
  };
}

export default function app(state=initialState, action) {
	switch (action.type) {
		case ActionTypes.App.INIT_APP: return initApp(state, action);
    case ActionTypes.App.UPDATE_USER: return updateUser(state, action);
		default: return state;
	}
}
