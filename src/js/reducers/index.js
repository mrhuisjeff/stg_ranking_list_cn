/**
 *  file name: index.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      This file combines all the reducers together
 */

import { combineReducers } from 'redux';

import app from './app';
import brands from './brands';
import games from './games';
import news from './news';
import ranks from './ranks';

export default combineReducers({
    app,
	brands,
    games,
    news,
    ranks
});
