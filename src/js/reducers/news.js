/**
 *  file name: news.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      reducer for games & brands
 */

import ActionTypes from '../constants';

function updateNews(state, action) {
    return [
        ...state,
        ...action.news,
    ];
}

export default function news(state=[], action) {
	switch (action.type) {
		case ActionTypes.News.UPDATE_NEWS: return updateNews(state, action);
		default: return state;
	};
}
