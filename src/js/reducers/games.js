/**
 *  file name: games.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      reducer for games & brands
 */

import ActionTypes from '../constants';
// import slugify from 'slug';
import { slugify } from '../libs/slug';

const defaultGameState = {
    state: {
        init: false,
        complete: false,
        page: null,
        ranks: []
    }
};

/*
{
    "id": "17",
    "brand_id": "7",
    "name": "怒首领蜂",
    "name_en": "Dodonpachi",
    "model": "Standard",
    "description": "",
    "image": "/images/game/cave_4.jpg",
    "source": "/images/game_rank/cave_4.jpg",
    "is_del": "\u0000",
    "type": "Type-A|Shot,Type-A|Laser,Type-B|Shot,Type-B|Laser,Type-C|Shot,Type-C|Laser",
    "rank": "0"
}
*/
function formatGame(game) {
    return {
        id: game.id,
        brandId: game.brand_id,
        name: game.name,
        nameEn: game.name_en,
        model: game.model,
        description: game.description,
        image: game.image,
        source: game.source,
        type: game.type,
        rank: game.rank,
        slug: slugify(game.name_en, {lower: true})
    };
}

function initGames(state, action) {
    const { games } = action;
    const formattedGames = Object.keys(games).reduce((gameMap, gameId) => {
        const game = games[gameId];
        const formatted = formatGame(game);
        gameMap[gameId] = {
            ...formatted,
            ...defaultGameState
        }
        return gameMap;
    }, {});
	return {
		...state,
		...formattedGames
	}
}

/*
state: {
    init: true,
    complete: ranks.length < ROWS_PER_PAGE,
    page,
    ranks: randIds
}
*/


function updateGameState(state, action) {
    const { gameId } = action;
    const game = state[gameId];
    return {
        ...state,
        [gameId]: {
            ...game,
            state: {
                ...action.state,
                ranks: game.state.ranks.concat(action.state.ranks)
            }
        }
    };
}


export default function games(state={}, action) {
	switch (action.type) {
		case ActionTypes.Game.INIT_GAMES: return initGames(state, action);
        case ActionTypes.Game.UPDATE_GAME_STATE: return updateGameState(state, action);
		default: return state;
	}
}
