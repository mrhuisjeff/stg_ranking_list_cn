/**
 *  file name: games.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      reducer for games & brands
 */

import ActionTypes from '../constants';
// import slugify from 'slug';
import { slugify } from '../libs/slug';

/*
{
    "id": "10",
    "is_del": "\u0000",
    "name": "Psikyo",
    "rank": "1",
    "description": "",
    "source": "/images/brand/psikyo_1.jpg",
    "games": []
}
*/

function formatBrand(brand) {
    return {
        id: brand.id,
        name: brand.name,
        rank: brand.rank,
        description: brand.description,
        source: brand.source,
        games: brand.games,
        slug: slugify(brand.name, {lower: true})
    };
}

/**
 	{
		type: games.UPDATE_BRANDS
		games: {
			brand1: {
				game1: [],
				...
			},
			...
		}
 	}
 */
function initBrands(state, action) {
    const { brands } = action;
    const formattedBrands = Object.keys(brands).reduce((map, brandId) => {
        const brand = brands[brandId];
        map[brandId] = formatBrand(brand);
        return map;
    }, {});
	return {
		...state,
		...formattedBrands
	}
}

export default function brands(state={}, action) {
	switch (action.type) {
		case ActionTypes.Brand.INIT_BRANDS: return initBrands(state, action);
		default: return state;
	}
}
