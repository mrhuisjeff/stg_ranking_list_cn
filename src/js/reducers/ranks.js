/**
 *  file name: ranks.js
 *  author: Gabriel Yum
 *  date: 2016-12-03
 *  usage:
 *      reducer for games & brands
 */

import ActionTypes from '../constants';

/*
{
    "id": "76",
    "score": "3544501000",
    "user_name": "王毅",
    "user_bid": "WY",
    "comment": "不拉掉HFD誓不罢休~",
    "stage": "ALL",
    "model": "黑版",
    "type": "Type-A|Expert",
    "loaction": "上海",
    "source": "",
    "time": "2013-02-12",
    "game_name": "怒首领蜂 大往生",
    "space_name": "街机"
},
 */

function formatRank(rank) {
    return {
        id: rank.id,
        gameId: rank.gameId,
        score: rank.score,
        userName: rank.user_name,
        userBid: rank.user_bid,
        comment: rank.comment,
        stage: rank.stage,
        model: rank.model,
        type: rank.type,
        location: rank.loaction,
        source: rank.source,
        time: rank.time,
        gameName: rank.game_name,
        spaceName: rank.space_name
    }
}


/**
 	{
		type: games.UPDATE_BRANDS
		games: {
			brand1: {
				game1: [],
				...
			},
			...
		}
 	}
 */
function updateRanks(state, action) {
    const { ranks } = action;
    const formattedRanks = Object.keys(ranks).reduce((map, rankId) => {
        const rank = ranks[rankId];
        map[rankId] = formatRank(rank);
        return map;
    }, {});

	return {
		...state,
		...formattedRanks
	}
}

export default function brands(state={}, action) {
	switch (action.type) {
		case ActionTypes.Rank.UPDATE_RANKS: return updateRanks(state, action);
		default: return state;
	}
}
